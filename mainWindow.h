#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "settings.h"
#include "themeInfo.h"

#include <QCursor>
#include <QEvent>
#include <QListWidget>
#include <QMainWindow>
#include <QPoint>
#include <QProcess>
#include <QScreen>
#include <QSettings>
#include <QSystemTrayIcon>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    void extracted();
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void onConfirm(const ThemeInfo &themeInfo);
    void on_pushButton_settings_clicked();
    void on_pushButton_lightMode_clicked();
    void on_pushButton_darkMode_clicked();

private:
    Ui::MainWindow *ui;

    QSystemTrayIcon *trayIcon;
    QPoint cursorPosition;
    Settings *settings;
    Backend backend;
    ThemeInfo themeInfo;

    void refreshTheme();
    void setVariantTheme();
    bool event(QEvent *event) override;
    void saveSystemSettings();
    void loadSystemSettings();
    void changeWhiskermenuOpacity();
    void changeIcon();
    void initTrayIcon();

signals:
    void openSettings(ThemeInfo &themeInfo);
};
#endif // MAINWINDOW_H
