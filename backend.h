#ifndef BACKEND_H
#define BACKEND_H

#include "cmd.h"
#include "creatorValues.h"
#include "themeInfo.h"

#include <QObject>

class Backend : public QObject
{
    Q_OBJECT

public:
    Backend();

    QStringList fetchThemes(QString &type);
    QStringList fetchIconThemes();
    void filterOutUnwantedIconThemes(QStringList &iconThemesList);
    QStringList fetchXfcePanelPlugins();
    QString fetchXfcePanelPluginName(const QStringList &plugins, const QString &searchedPluginName);
    void changeXfcePanelPluginProperty(const QString &pluginName, const QString &propertyName, const QString &propertyValue);
    void setTheme(const QString &type, const QString &theme);
    void setGtk4Theme(const QString &theme, const bool isDarkMode);
    void resetXfcePanel();
    void saveSystemSettings(const ThemeInfo &themeInfo);
    void loadSystemSettings(ThemeInfo &themeInfo);

    CreatorValues getCreatorValues() const;

private:
    Cmd cmd;
    CreatorValues creatorValues;

    QStringList processAndSortUniqueList(QString &concatString);
    QString fetchXfcePanelPropertyValue(const QString &pluginName, const QString &propertyName);
    void saveAppSystemSettings(const ThemeInfo &themeInfo);
    void saveQtCreatorSystemSettings(const ThemeInfo &themeInfo);

    QString getCurrentGtk3ThemeName();
    QString getCurrentGtk4ThemeName();
    QString getCurrentXfwm4ThemeName();
    QString getCurrentIconThemeName();
    int getWhiskerMenuOpacityValue();
};

#endif // BACKEND_H
