#include "backend.h"

#include <QDir>

Backend::Backend()
{
}

QStringList Backend::fetchThemes(QString &type)
{
    QString themes;

    themes = cmd.run("find /usr/share/themes/*/" + type + " -maxdepth 0 2>/dev/null|cut -d'/' -f5").output;
    themes.append("\n");
    themes.append(cmd.run("find $HOME/.themes/*/" + type + " -maxdepth 0 2>/dev/null|cut -d'/' -f5").output);
    themes.append("\n");
    themes.append(cmd.run("find $HOME/.local/share/themes/*/" + type + " -maxdepth 0 2>/dev/null|cut -d'/' -f7").output);

    return processAndSortUniqueList(themes);
}

QStringList Backend::fetchIconThemes()
{
    QString themes;

    themes = cmd.run("find /usr/share/icons/*/index.theme -maxdepth 1 2>/dev/null|cut -d'/' -f5").output;
    themes.append("\n");
    themes.append(cmd.run("find $HOME/.icons/*/index.theme -maxdepth 1 2>/dev/null|cut -d'/' -f5").output);
    themes.append("\n");
    themes.append(cmd.run("find $HOME/.local/share/icons/*/index.theme -maxdepth 1 2>/dev/null|cut -d'/' -f7").output);

    return processAndSortUniqueList(themes);
}

void Backend::filterOutUnwantedIconThemes(QStringList &iconThemesList)
{
    for (const QString &item : iconThemesList) {
        QString result = cmd.run("find /usr/share/icons/" + item + " -maxdepth 1 -mindepth 1 -type d |cut -d'/' -f6").output;

        if (result == QLatin1String("cursors")) {
            iconThemesList.removeAll(item);
        }
    }

    iconThemesList.removeAll("default.kde4");
    iconThemesList.removeAll("default");
    iconThemesList.removeAll("hicolor");
}

QStringList Backend::fetchXfcePanelPlugins()
{
    Result result = cmd.run("xfconf-query -c xfce4-panel -p / -l | grep '^/plugins/plugin-' | awk -F'/' '{print $3}' | sort -u");
    return result.output.split('\n', Qt::SkipEmptyParts);
}

QString Backend::fetchXfcePanelPluginName(const QStringList &plugins, const QString &searchedPluginName)
{
    for (const QString& plugin : plugins) {
        Result pluginResult = cmd.run(QString("xfconf-query -c xfce4-panel -p /plugins/%1").arg(plugin));
        QString value = pluginResult.output.trimmed();

        if (value == searchedPluginName) {
            return plugin;
        }
    }

    return QString();
}

void Backend::changeXfcePanelPluginProperty(const QString &pluginName, const QString &propertyName, const QString &propertyValue)
{
    cmd.run(QString("xfconf-query -c xfce4-panel -p /plugins/%1/%2 -s %3").arg(pluginName, propertyName, propertyValue));
}

void Backend::setTheme(const QString &type, const QString &theme)
{
    QString command;

    if (type == QLatin1String("gtk-3.0")) {
        command = "xfconf-query -c xsettings -p /Net/ThemeName -s \"" + theme + "\"";
    }

    if (type == QLatin1String("xfwm4")) {
        command = "xfconf-query -c xfwm4 -p /general/theme -s \"" + theme + "\"";
    }

    if (type == QLatin1String("icon")) {
        command = "xfconf-query -c xsettings -p /Net/IconThemeName -s \"" + theme + "\"";
    }

    cmd.run(command);
}

void Backend::setGtk4Theme(const QString &theme, const bool isDarkMode)
{
    QDir configDir = QDir::homePath() + "/.config/gtk-4.0";
    configDir.removeRecursively();

    QStringList searchPaths = {
        "/usr/share/themes/",
        QDir::homePath() + "/.themes/",
        QDir::homePath() + "/.local/share/themes/"
    };

    for(const QString &searchPath : searchPaths) {
        QDir dir(searchPath + theme + "/gtk-4.0");
        if(dir.exists()) {
            QString linkPath = QDir::homePath() + "/.config";
            QString linkCommand = "cp -rs " + dir.path() + " " + linkPath;
            cmd.run(linkCommand);
            return;
        }
    }
}

void Backend::resetXfcePanel()
{
    cmd.run("xfce4-panel -r");
}

void Backend::saveSystemSettings(const ThemeInfo &themeInfo)
{
    saveAppSystemSettings(themeInfo);
    saveQtCreatorSystemSettings(themeInfo);
}

void Backend::loadSystemSettings(ThemeInfo &themeInfo)
{
    QSettings systemSettings(themeInfo.getQSettingsLocation(), themeInfo.getAppName());

    systemSettings.beginGroup("Settings");

    QString gtk3Theme = getCurrentGtk3ThemeName();
    QString gtk4Theme = getCurrentGtk4ThemeName();
    QString xfwm4Theme = getCurrentXfwm4ThemeName();
    QString iconTheme = getCurrentIconThemeName();
    int menuOpacity = getWhiskerMenuOpacityValue();
    bool themeMode = gtk3Theme.contains("dark", Qt::CaseInsensitive);

    themeInfo.setThemeInfo(systemSettings, themeInfo, menuOpacity, themeMode, gtk3Theme, gtk4Theme, xfwm4Theme, iconTheme);

    systemSettings.endGroup();
}

CreatorValues Backend::getCreatorValues() const
{
    return creatorValues;
}

QStringList Backend::processAndSortUniqueList(QString &concatString)
{
    QStringList resultList;

    resultList = concatString.split("\n");
    resultList.removeDuplicates();
    resultList.removeAll(QLatin1String(""));
    resultList.sort(Qt::CaseInsensitive);

    return resultList;
}

QString Backend::fetchXfcePanelPropertyValue(const QString &pluginName, const QString &propertyName)
{
    Result result = cmd.run(QString("xfconf-query -c xfce4-panel -p /plugins/%1/%2").arg(pluginName, propertyName));
    return result.output.trimmed();
}

void Backend::saveAppSystemSettings(const ThemeInfo &themeInfo)
{
    QSettings systemSettings(themeInfo.getQSettingsLocation(), themeInfo.getAppName());

    systemSettings.beginGroup("Settings");

    systemSettings.setValue("lightThemeGtk3Name", themeInfo.getLightThemeGtk3Name());
    systemSettings.setValue("lightThemeGtk4Name", themeInfo.getLightThemeGtk4Name());
    systemSettings.setValue("lightThemeWMName", themeInfo.getLightThemeWMName());
    systemSettings.setValue("lightIconThemeName", themeInfo.getLightIconThemeName());
    systemSettings.setValue("darkThemeGtk3Name", themeInfo.getDarkThemeGtk3Name());
    systemSettings.setValue("darkThemeGtk4Name", themeInfo.getDarkThemeGtk4Name());
    systemSettings.setValue("darkThemeWMName", themeInfo.getDarkThemeWMName());
    systemSettings.setValue("darkIconThemeName", themeInfo.getDarkIconThemeName());
    systemSettings.setValue("qtCreatorLightThemeName", themeInfo.getQtCreatorLightThemeName());
    systemSettings.setValue("qtCreatorDarkThemeName", themeInfo.getQtCreatorDarkThemeName());
    systemSettings.setValue("whiskermenuOpacityLight", themeInfo.getWhiskermenuOpacityLight());
    systemSettings.setValue("whiskermenuOpacityDark", themeInfo.getWhiskermenuOpacityDark());
    systemSettings.setValue("isDarkMode", themeInfo.getIsDarkMode());
    systemSettings.setValue("isActiveQtCreatorTheme", themeInfo.getIsActiveQtCreatorTheme());

    systemSettings.endGroup();
}

void Backend::saveQtCreatorSystemSettings(const ThemeInfo &themeInfo)
{
    QString qtCreatorFilePath = QDir::homePath() + "/.config/QtProject/QtCreator.ini";
    QSettings qtCreatorSettings(qtCreatorFilePath, QSettings::IniFormat);

    qtCreatorSettings.beginGroup("Core");

    if (themeInfo.getIsActiveQtCreatorTheme()) {
        if (themeInfo.getIsDarkMode()) {
            qtCreatorSettings.setValue("CreatorTheme", themeInfo.getQtCreatorDarkThemeName());
        } else {
            qtCreatorSettings.setValue("CreatorTheme", themeInfo.getQtCreatorLightThemeName());
        }
    }

    qtCreatorSettings.endGroup();
}

QString Backend::getCurrentGtk3ThemeName()
{
    return cmd.run("xfconf-query -c xsettings -p /Net/ThemeName").output;
}

QString Backend::getCurrentGtk4ThemeName()
{
    return cmd.run("xfconf-query -c xsettings -p /Net/ThemeName").output;
}

QString Backend::getCurrentXfwm4ThemeName()
{
    return cmd.run("xfconf-query -c xfwm4 -p /general/theme").output;
}

QString Backend::getCurrentIconThemeName()
{
    return cmd.run("xfconf-query -c xsettings -p /Net/IconThemeName").output;
}

int Backend::getWhiskerMenuOpacityValue()
{
    QStringList plugins = fetchXfcePanelPlugins();
    QString whiskermenuPlugin = fetchXfcePanelPluginName(plugins, "whiskermenu");

    if (!whiskermenuPlugin.isEmpty()) {
        return fetchXfcePanelPropertyValue(whiskermenuPlugin, "menu-opacity").toInt();
    }

    return -1;
}
