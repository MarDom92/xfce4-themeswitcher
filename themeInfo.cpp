#include "themeInfo.h"

ThemeInfo::ThemeInfo()
{
    appName = "Xfce4 ThemeSwitcher";
    qSettingsLocation = "xfce4/Xfce4-ThemeSwitcher";
}

QString ThemeInfo::getLightThemeGtk3Name() const
{
    return lightThemeGtk3Name;
}

QString ThemeInfo::getLightThemeGtk4Name() const
{
    return lightThemeGtk4Name;
}

QString ThemeInfo::getLightThemeWMName() const
{
    return lightThemeWMName;
}

QString ThemeInfo::getLightIconThemeName() const
{
    return lightIconThemeName;
}

QString ThemeInfo::getQtCreatorLightThemeName() const
{
    return qtCreatorLightThemeName;
}

QString ThemeInfo::getDarkThemeGtk3Name() const
{
    return darkThemeGtk3Name;
}

QString ThemeInfo::getDarkThemeGtk4Name() const
{
    return darkThemeGtk4Name;
}

QString ThemeInfo::getDarkThemeWMName() const
{
    return darkThemeWMName;
}

QString ThemeInfo::getDarkIconThemeName() const
{
    return darkIconThemeName;
}

QString ThemeInfo::getQtCreatorDarkThemeName() const
{
    return qtCreatorDarkThemeName;
}

int ThemeInfo::getWhiskermenuOpacityLight() const
{
    return whiskermenuOpacityLight;
}

int ThemeInfo::getWhiskermenuOpacityDark() const
{
    return whiskermenuOpacityDark;
}

bool ThemeInfo::getIsDarkMode() const
{
    return isDarkMode;
}

bool ThemeInfo::getIsActiveQtCreatorTheme() const
{
    return isActiveQtCreatorTheme;
}

void ThemeInfo::setThemeInfo(const QSettings &systemSettings,
                             ThemeInfo &themeInfo,
                             int menuOpacity,
                             bool themeMode,
                             QString gtk3Theme,
                             QString gtk4Theme,
                             QString xfwm4Theme,
                             QString iconTheme)
{
    themeInfo.setLightThemeGtk3Name(
                systemSettings.value("lightThemeGtk3Name", gtk3Theme).toString());
    themeInfo.setLightThemeGtk4Name(
                systemSettings.value("lightThemeGtk4Name", gtk4Theme).toString());
    themeInfo.setLightThemeWMName(
                systemSettings.value("lightThemeWMName", xfwm4Theme).toString());
    themeInfo.setLightIconThemeName(
                systemSettings.value("lightIconThemeName", iconTheme).toString());
    themeInfo.setQtCreatorLightThemeName(
                systemSettings.value("qtCreatorLightThemeName").toString());
    themeInfo.setDarkThemeGtk3Name(
                systemSettings.value("darkThemeGtk3Name", gtk3Theme).toString());
    themeInfo.setDarkThemeGtk4Name(
                systemSettings.value("darkThemeGtk4Name", gtk4Theme).toString());
    themeInfo.setDarkThemeWMName(
                systemSettings.value("darkThemeWMName", xfwm4Theme).toString());
    themeInfo.setDarkIconThemeName(
                systemSettings.value("darkIconThemeName", iconTheme).toString());
    themeInfo.setQtCreatorDarkThemeName(
                systemSettings.value("qtCreatorDarkThemeName").toString());
    themeInfo.setWhiskermenuOpacityLight(
                systemSettings.value("whiskermenuOpacityLight", menuOpacity).toInt());
    themeInfo.setWhiskermenuOpacityDark(
                systemSettings.value("whiskermenuOpacityDark", menuOpacity).toInt());
    themeInfo.setIsDarkMode(
                systemSettings.value("isDarkMode", themeMode).toBool());
    themeInfo.setIsActiveQtCreatorTheme(
                systemSettings.value("isActiveQtCreatorTheme").toBool());
}

void ThemeInfo::setLightThemeGtk3Name(const QString &value)
{
    lightThemeGtk3Name = value;
}

void ThemeInfo::setLightThemeGtk4Name(const QString &value)
{
    lightThemeGtk4Name = value;
}

void ThemeInfo::setLightThemeWMName(const QString &value)
{
    lightThemeWMName = value;
}

void ThemeInfo::setLightIconThemeName(const QString &value)
{
    lightIconThemeName = value;
}

void ThemeInfo::setQtCreatorLightThemeName(const QString &value)
{
    qtCreatorLightThemeName = value;
}

void ThemeInfo::setDarkThemeGtk3Name(const QString &value)
{
    darkThemeGtk3Name = value;
}

void ThemeInfo::setDarkThemeGtk4Name(const QString &value)
{
    darkThemeGtk4Name = value;
}

void ThemeInfo::setDarkThemeWMName(const QString &value)
{
    darkThemeWMName = value;
}

void ThemeInfo::setDarkIconThemeName(const QString &value)
{
    darkIconThemeName = value;
}

void ThemeInfo::setQtCreatorDarkThemeName(const QString &value)
{
    qtCreatorDarkThemeName = value;
}

void ThemeInfo::setWhiskermenuOpacityLight(int value)
{
    whiskermenuOpacityLight = value;

}

void ThemeInfo::setWhiskermenuOpacityDark(int value)
{
    whiskermenuOpacityDark = value;
}

void ThemeInfo::setIsDarkMode(bool value)
{
    isDarkMode = value;
}

void ThemeInfo::setIsActiveQtCreatorTheme(const bool &value)
{
    isActiveQtCreatorTheme = value;
}

QString ThemeInfo::getAppName() const
{
    return appName;
}

QString ThemeInfo::getQSettingsLocation() const
{
    return qSettingsLocation;
}
