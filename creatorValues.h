#ifndef CREATORVALUES_H
#define CREATORVALUES_H

#include <QMap>
#include <QString>

class CreatorValues
{
public:
    CreatorValues();

    QMap<QString, QString> getThemeMap() const;
    void setThemeMap(const QMap<QString, QString> &newThemeMap);

private:
    QMap<QString, QString> themeMap;
};

#endif // CREATORVALUES_H
