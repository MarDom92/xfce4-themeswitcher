#ifndef CMD_H
#define CMD_H

#include <QObject>
#include <QString>
#include <QProcess>
#include <QEventLoop>

struct Result {
    int exitCode;
    QString output;
};

class Cmd : public QObject {
    Q_OBJECT

public:
    QEventLoop loop;
    QProcess proc;

    Result run(const QString &cmd);

private slots:
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);
};

#endif // CMD_H
