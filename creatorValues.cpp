#include "creatorValues.h"

CreatorValues::CreatorValues()
{
    themeMap = {{"Flat", ""},
                {"Flat Dark", "dark"},
                {"Classic", "default"},
                {"Design Light", "design-light"},
                {"Design Dark", "design-dark"},
                {"Flat Light", "flat-light"},
                {"Flat Dark", "flat-dark"}};
}

QMap<QString, QString> CreatorValues::getThemeMap() const
{
    return themeMap;
}

void CreatorValues::setThemeMap(const QMap<QString, QString> &newThemeMap)
{
    themeMap = newThemeMap;
}
