#include "cmd.h"

Result Cmd::run(const QString &cmd)
{
    proc.setProcessChannelMode(QProcess::MergedChannels);
    QObject::connect(&proc, &QProcess::finished, this, &Cmd::processFinished);
    proc.start(QStringLiteral("/bin/bash"), {QStringLiteral("-c"), cmd});

    loop.exec();

    return {proc.exitCode(), proc.readAll().trimmed()};
}

void Cmd::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    loop.quit();
}
