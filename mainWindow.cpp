#include "mainWindow.h"

#include "ui_mainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loadSystemSettings();
    initTrayIcon();

    if (themeInfo.getIsDarkMode()) {
        ui->pushButton_lightMode->setChecked(false);
        ui->pushButton_darkMode->setChecked(true);
    } else {
        ui->pushButton_lightMode->setChecked(true);
        ui->pushButton_darkMode->setChecked(false);
    }

    settings = new Settings(this, themeInfo.getIsDarkMode());

    connect(settings, &Settings::closeSettingsWithChanges, this, &MainWindow::onConfirm);
    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::iconActivated);
    connect(this, &MainWindow::openSettings, settings, &Settings::showSettings);

    setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
}

MainWindow::~MainWindow()
{
    saveSystemSettings();
    changeWhiskermenuOpacity();

    delete ui;
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (settings && settings->isVisible()) {
        return;
    }

    if (reason == QSystemTrayIcon::Trigger) {

        int windowWidth = this->width() / 2;
        this->move(QCursor::pos() - QPoint(windowWidth, 0));

        if (this->isVisible()) {
            this->hide();
        } else {
            this->show();
        }
    }
}

void MainWindow::onConfirm(const ThemeInfo &themeInfo)
{
    this->themeInfo = themeInfo;
    refreshTheme();
}

void MainWindow::on_pushButton_settings_clicked()
{
    this->hide();
    emit openSettings(themeInfo);
}

void MainWindow::on_pushButton_lightMode_clicked()
{
    if (themeInfo.getIsDarkMode()) {
        themeInfo.setIsDarkMode(false);
        ui->pushButton_darkMode->setChecked(false);
        refreshTheme();
    }

    ui->pushButton_lightMode->setChecked(true);
    this->hide();
}

void MainWindow::on_pushButton_darkMode_clicked()
{
    if (!themeInfo.getIsDarkMode()) {
        themeInfo.setIsDarkMode(true);
        ui->pushButton_lightMode->setChecked(false);
        refreshTheme();
    }

    ui->pushButton_darkMode->setChecked(true);
    this->hide();
}

void MainWindow::refreshTheme()
{
    setVariantTheme();
    changeIcon();
    backend.resetXfcePanel();
    saveSystemSettings();
    changeWhiskermenuOpacity();
}

void MainWindow::setVariantTheme()
{
    if (themeInfo.getIsDarkMode()) {
        backend.setTheme("gtk-3.0", themeInfo.getDarkThemeGtk3Name());
        backend.setGtk4Theme(themeInfo.getDarkThemeGtk4Name(), themeInfo.getIsDarkMode());
        backend.setTheme("xfwm4", themeInfo.getDarkThemeWMName());
        backend.setTheme("icon", themeInfo.getDarkIconThemeName());
    } else {
        backend.setTheme("gtk-3.0", themeInfo.getLightThemeGtk3Name());
        backend.setGtk4Theme(themeInfo.getLightThemeGtk4Name(), themeInfo.getIsDarkMode());
        backend.setTheme("xfwm4", themeInfo.getLightThemeWMName());
        backend.setTheme("icon", themeInfo.getLightIconThemeName());
    }
}

bool MainWindow::event(QEvent *event)
{
    if (event->type() == QEvent::ActivationChange) {
        if (!isActiveWindow()) {
            hide();
        }
    }

    return QMainWindow::event(event);
}

void MainWindow::saveSystemSettings()
{
    backend.saveSystemSettings(themeInfo);


}

void MainWindow::loadSystemSettings()
{
    backend.loadSystemSettings(themeInfo);
}

void MainWindow::changeWhiskermenuOpacity()
{
    QStringList plugins = backend.fetchXfcePanelPlugins();
    QString whiskermenuPlugin = backend.fetchXfcePanelPluginName(plugins, "whiskermenu");
    if(themeInfo.getIsDarkMode()) {
        backend.changeXfcePanelPluginProperty(whiskermenuPlugin, "menu-opacity", QString::number(themeInfo.getWhiskermenuOpacityDark()));
    } else {
        backend.changeXfcePanelPluginProperty(whiskermenuPlugin, "menu-opacity", QString::number(themeInfo.getWhiskermenuOpacityLight()));
    }
}

void MainWindow::changeIcon()
{
    if (themeInfo.getIsDarkMode()) {
        trayIcon->setIcon(QIcon::fromTheme("night-light-symbolic", QIcon(":/yin-yan-symbol-dark-theme.png")));
    } else {
        trayIcon->setIcon(QIcon::fromTheme("night-light-symbolic", QIcon(":/yin-yan-symbol-light-theme.png")));
    }
}

void MainWindow::initTrayIcon()
{
    trayIcon = new QSystemTrayIcon(this);
    changeIcon();
    trayIcon->setVisible(true);
}
