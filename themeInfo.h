#ifndef THEMEINFO_H
#define THEMEINFO_H

#include <QSettings>
#include <QString>

class ThemeInfo
{
public:
    ThemeInfo();

    QString getLightThemeGtk3Name() const;
    QString getLightThemeGtk4Name() const;
    QString getLightThemeWMName() const;
    QString getLightIconThemeName() const;
    QString getQtCreatorLightThemeName() const;

    QString getDarkThemeGtk3Name() const;
    QString getDarkThemeGtk4Name() const;
    QString getDarkThemeWMName() const;
    QString getDarkIconThemeName() const;
    QString getQtCreatorDarkThemeName() const;

    int getWhiskermenuOpacityLight() const;
    int getWhiskermenuOpacityDark() const;
    bool getIsDarkMode() const;
    bool getIsActiveQtCreatorTheme() const;

    void setThemeInfo(const QSettings &systemSettings,
                      ThemeInfo &themeInfo,
                      int menuOpacity,
                      bool themeMode,
                      QString gtk3Theme,
                      QString gtk4Theme,
                      QString xfwm4Theme,
                      QString iconTheme);

    void setLightThemeGtk3Name(const QString &value);
    void setLightThemeGtk4Name(const QString &value);
    void setLightThemeWMName(const QString &value);
    void setLightIconThemeName(const QString &value);
    void setQtCreatorLightThemeName(const QString &value);
    void setDarkThemeGtk3Name(const QString &value);
    void setDarkThemeGtk4Name(const QString &value);
    void setDarkThemeWMName(const QString &value);
    void setDarkIconThemeName(const QString &value);
    void setQtCreatorDarkThemeName(const QString &value);
    void setWhiskermenuOpacityLight(int value);
    void setWhiskermenuOpacityDark(int value);
    void setIsDarkMode(bool value);
    void setIsActiveQtCreatorTheme(const bool &value);

    QString getAppName() const;
    QString getQSettingsLocation() const;

private:
    QString lightThemeGtk3Name;
    QString lightThemeGtk4Name;
    QString lightThemeWMName;
    QString lightIconThemeName;
    QString qtCreatorLightThemeName;

    QString darkThemeGtk3Name;
    QString darkThemeGtk4Name;
    QString darkThemeWMName;
    QString darkIconThemeName;
    QString qtCreatorDarkThemeName;

    int whiskermenuOpacityLight;
    int whiskermenuOpacityDark;

    bool isDarkMode;
    bool isActiveQtCreatorTheme;

    QString appName;
    QString qSettingsLocation;
};

#endif // THEMEINFO_H
