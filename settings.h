#ifndef SETTINGS_H
#define SETTINGS_H

#include "themeInfo.h"
#include "backend.h"

#include <QDialog>
#include <QListWidget>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QStringList>
#include <QSettings>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr, bool darkMode = false);
    ~Settings();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::Settings *ui;
    Settings *settings;

    Backend backend;
    ThemeInfo themeInfo;

    void setupFields();
    void populateGtk3ThemesList();
    void populateGtk4ThemesList();
    void populateXfwm4ThemesList();
    void populateIconsThemesList();
    void populateQtCreatorThemes();
    void setWhiskerMenuOpacity();

    static void setCurrentItemInQListWidget(QListWidget *list, const QString themeName);

public slots:
    void showSettings(const ThemeInfo &themeInfo);

private slots:
    void on_listWidget_lightThemeGtk3_currentTextChanged(const QString &value);
    void on_listWidget_lightThemeGtk4_currentTextChanged(const QString &currentText);
    void on_listWidget_lightWMTheme_currentTextChanged(const QString &value);
    void on_listWidget_lightIconsTheme_currentTextChanged(const QString &value);
    void on_listWidget_darkThemeGtk3_currentTextChanged(const QString &value);
    void on_listWidget_darkThemeGtk4_currentTextChanged(const QString &currentText);
    void on_listWidget_darkWMTheme_currentTextChanged(const QString &value);
    void on_listWidget_darkIconsTheme_currentTextChanged(const QString &value);

    void on_spinBox_whiskermenuOpacityLight_valueChanged(int value);
    void on_spinBox_whiskermenuOpacityDark_valueChanged(int value);
    void on_checkBox_qtCreator_stateChanged(int value);
    void on_comboBox_qtCreatorDarkTheme_currentTextChanged(const QString &value);
    void on_comboBox_qtCreatorLightTheme_currentTextChanged(const QString &value);

    void on_pushButton_confirm_clicked();
    void on_pushButton_cancel_clicked();

signals:
    void closeSettingsWithChanges(const ThemeInfo &themeInfo);
};

#endif // SETTINGS_H
