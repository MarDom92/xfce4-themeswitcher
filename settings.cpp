#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent, bool darkMode)
    : QDialog(parent),
      ui(new Ui::Settings)
{
    ui->setupUi(this);

    this->setModal(true);

    populateGtk3ThemesList();
    populateGtk4ThemesList();
    populateXfwm4ThemesList();
    populateIconsThemesList();
    populateQtCreatorThemes();

    setWhiskerMenuOpacity();

    this->setWindowTitle(themeInfo.getAppName());
    this->adjustSize();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::setupFields()
{
    setCurrentItemInQListWidget(ui->listWidget_lightThemeGtk3, themeInfo.getLightThemeGtk3Name());
    setCurrentItemInQListWidget(ui->listWidget_darkThemeGtk3, themeInfo.getDarkThemeGtk3Name());
    setCurrentItemInQListWidget(ui->listWidget_lightThemeGtk4, themeInfo.getLightThemeGtk4Name());
    setCurrentItemInQListWidget(ui->listWidget_darkThemeGtk4, themeInfo.getDarkThemeGtk4Name());
    setCurrentItemInQListWidget(ui->listWidget_lightWMTheme, themeInfo.getLightThemeWMName());
    setCurrentItemInQListWidget(ui->listWidget_darkWMTheme, themeInfo.getDarkThemeWMName());
    setCurrentItemInQListWidget(ui->listWidget_lightIconsTheme, themeInfo.getLightIconThemeName());
    setCurrentItemInQListWidget(ui->listWidget_darkIconsTheme, themeInfo.getDarkIconThemeName());

    ui->spinBox_whiskermenuOpacityLight->setValue(themeInfo.getWhiskermenuOpacityLight());
    ui->spinBox_whiskermenuOpacityDark->setValue(themeInfo.getWhiskermenuOpacityDark());
    ui->checkBox_qtCreator->setChecked(themeInfo.getIsActiveQtCreatorTheme());
    ui->comboBox_qtCreatorLightTheme->setCurrentText(backend.getCreatorValues().getThemeMap().key(themeInfo.getQtCreatorLightThemeName()));
    ui->comboBox_qtCreatorDarkTheme->setCurrentText(backend.getCreatorValues().getThemeMap().key(themeInfo.getQtCreatorDarkThemeName()));
}

void Settings::populateGtk3ThemesList()
{
    QString type = "gtk-3.0";
    QStringList themeList = backend.fetchThemes(type);

    ui->listWidget_lightThemeGtk3->clear();
    ui->listWidget_lightThemeGtk3->addItems(themeList);
    ui->listWidget_darkThemeGtk3->clear();
    ui->listWidget_darkThemeGtk3->addItems(themeList);
}

void Settings::populateGtk4ThemesList()
{
    QString type = "gtk-3.0";
    QStringList themeList = backend.fetchThemes(type);

    ui->listWidget_lightThemeGtk4->clear();
    ui->listWidget_lightThemeGtk4->addItems(themeList);
    ui->listWidget_darkThemeGtk4->clear();
    ui->listWidget_darkThemeGtk4->addItems(themeList);
}

void Settings::populateXfwm4ThemesList()
{
    QString type = "xfwm4";
    QStringList themeList = backend.fetchThemes(type);

    ui->listWidget_lightWMTheme->clear();
    ui->listWidget_lightWMTheme->addItems(themeList);
    ui->listWidget_darkWMTheme->clear();
    ui->listWidget_darkWMTheme->addItems(themeList);
}

void Settings::populateIconsThemesList()
{

    QStringList iconThemesList = backend.fetchIconThemes();
    backend.filterOutUnwantedIconThemes(iconThemesList);

    ui->listWidget_lightIconsTheme->clear();
    ui->listWidget_lightIconsTheme->addItems(iconThemesList);
    ui->listWidget_darkIconsTheme->clear();
    ui->listWidget_darkIconsTheme->addItems(iconThemesList);
}

void Settings::populateQtCreatorThemes()
{
    QStringList qtCreatorThemes = backend.getCreatorValues().getThemeMap().keys();

    ui->comboBox_qtCreatorLightTheme->addItems(qtCreatorThemes);
    ui->comboBox_qtCreatorDarkTheme->addItems(qtCreatorThemes);
}

void Settings::setWhiskerMenuOpacity()
{
    ui->spinBox_whiskermenuOpacityLight->setValue(themeInfo.getWhiskermenuOpacityLight());
    ui->spinBox_whiskermenuOpacityLight->setValue(themeInfo.getWhiskermenuOpacityDark());
}

void Settings::setCurrentItemInQListWidget(QListWidget *list, const QString themeName)
{
    for (int i = 0; i < list->count(); ++i) {
        QListWidgetItem *item = list->item(i);

        if (themeName == item->text()) {
            list->setCurrentItem(item);
        }
    }
}

void Settings::closeEvent(QCloseEvent *event) {
    this->close();
}

void Settings::showSettings(const ThemeInfo &themeInfo)
{
    this->show();
    this->themeInfo = themeInfo;

    setupFields();
}

void Settings::on_listWidget_lightThemeGtk3_currentTextChanged(const QString &value)
{
    themeInfo.setLightThemeGtk3Name(value);
}

void Settings::on_listWidget_lightThemeGtk4_currentTextChanged(const QString &value)
{
    themeInfo.setLightThemeGtk4Name(value);
}

void Settings::on_listWidget_lightWMTheme_currentTextChanged(const QString &value)
{
    themeInfo.setLightThemeWMName(value);
}

void Settings::on_listWidget_lightIconsTheme_currentTextChanged(const QString &value)
{
    themeInfo.setLightIconThemeName(value);
}

void Settings::on_listWidget_darkThemeGtk3_currentTextChanged(const QString &value)
{
    themeInfo.setDarkThemeGtk3Name(value);
}

void Settings::on_listWidget_darkThemeGtk4_currentTextChanged(const QString &value)
{
    themeInfo.setDarkThemeGtk4Name(value);
}

void Settings::on_listWidget_darkWMTheme_currentTextChanged(const QString &value)
{
    themeInfo.setDarkThemeWMName(value);
}

void Settings::on_listWidget_darkIconsTheme_currentTextChanged(const QString &value)
{
    themeInfo.setDarkIconThemeName(value);
}

void Settings::on_spinBox_whiskermenuOpacityLight_valueChanged(int value)
{
    themeInfo.setWhiskermenuOpacityLight(value);
}

void Settings::on_spinBox_whiskermenuOpacityDark_valueChanged(int value)
{
    themeInfo.setWhiskermenuOpacityDark(value);
}

void Settings::on_checkBox_qtCreator_stateChanged(int value)
{
    if (value == Qt::Checked) {
        themeInfo.setIsActiveQtCreatorTheme(true);
    } else {
        themeInfo.setIsActiveQtCreatorTheme(false);
    }
}

void Settings::on_comboBox_qtCreatorLightTheme_currentTextChanged(const QString &value)
{
    themeInfo.setQtCreatorLightThemeName(backend.getCreatorValues().getThemeMap().value(value));
}

void Settings::on_comboBox_qtCreatorDarkTheme_currentTextChanged(const QString &value)
{
    themeInfo.setQtCreatorDarkThemeName(backend.getCreatorValues().getThemeMap().value(value));
}

void Settings::on_pushButton_confirm_clicked()
{
    this->close();
    emit closeSettingsWithChanges(themeInfo);
}

void Settings::on_pushButton_cancel_clicked()
{
    this->close();
}
